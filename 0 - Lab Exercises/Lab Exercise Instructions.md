## Theme

The goal of this workshop is to give you a look into some of the features the GitLab AI team is developing, not just Code Suggestions. We've put a big emphasis on helping developers throughout the entire SDLC and not _just_ coding tasks.

**PLEASE NOTE:** Many of these features are in the [experimental phase](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#experiment), which means they may be prone to outages as the dev team is actively working on enhancing them. If this occurs and you didn't see a feature demo live that you hopped to see, please reach out to the account team and we will provide recordings & additional info on when it may be available.

## Key Tasks to Complete


* [ ] **Step 1: Pipeline Kickoff & GitLab Chat**
* ![gitlab_chat.svg](./gitlab_chat.svg)
  * Use the sidebar to click through **Build \> Pipelines** and click **Run pipeline** in the top right.
  * Make sure the **_main_** branch is selected and click **Run pipeline**. We will let this pipeline run and come back later in the workshop.
  * _What if we hadn't known where to access the pipelines from or how to open an MR?_ You can now use the new AI chat feature called [GitLab Duo Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html). Go ahead and click the **? Help** button in the bottom left then select **GitLab Duo Chat**
  * You can ask GitLab Duo any related GitLab questions such as: "**Where can I find my running pipelines?**", to which the chat will respond with the path to the pipelines page. Other examples of questions you can ask are:
    * "**What is a fork?**"
    * "**Write a tic tac toe game in Javascript"**
    * "**Generate a summary for the issue identified via this link: <link to your issue>**"
  * While the pipeline runs, take some time to go through [the current AI/ML features GitLab offers](https://docs.gitlab.com/ee/user/ai_features.html)

    > Please note that asking for code suggestions can take a few minutes so it is best to test out post workshop. Additionally, keep in mind that you will have a short trial period post workshop with access to this group that has all of the features enabled, so feel free to try any out that we do not directly cover during the workshop.

  ---
* [ ] **Step 2: Security Results & AI**
* ![explain_this_vuln.svg](./explain_this_vuln.svg) 
  * Next use the left hand navigation menu to click through **Build \> Pipelines** and ensure that the most recent pipeline we kicked off is complete. Select the most recent pipeline that completed (unique number preceded by #).
  * Spend some time taking a look at all of the information provided to you, exploring the job results, security tab, and other informative tabs.
  * We have already seen how to view the vulnerabilities in the pipeline view, but now lets use the left hand navigation menu and click through **Secure -\> Vulnerability report** to view the full report
  * Once in the **_Vulnerability Report_** we first can click into any of the vulnerabilities present.
  * Next look for the **Improper Neutralization of Special Elements used in an SQL Command ('SQL Injection')** or **_CWE-89 in db.py_** vulnerability and click into one of them. We can see that this vulnerability was triggered by a SQL injection risk. Scroll down and click the **Explain vulnerability** button for an explanation on what a SQL injection risk is and why our application is vulnerable.
  
  * At the end of the report check out the **_Fixed_** code section and we can see that if we paramaterize the query by replacing  `cur.execute(query)`\_ with `cur.execute(query, (user_id,))` it will prevent the possibility of sql injection. We will use this knowledge in the next exercise.

Note: there may be other valid solutions that are suggested by the AI.

  ---
* [ ] **Step 3: Explain This Code**
* ![explain_this_code.svg](./explain_this_code.svg)
  * What if we wanted more context about the specific function above before we went and made a code change? Let's take a closer look at the class in our IDE to understand what it's doing.  Use the **Code > Repository** menu to get to the source files, then click the **notes > db.py** file to view its contents. Scroll down to line 111.  
  * Once within the db.py file locate the line the sql injection vulnerability was occurring on and highlight the entire function.
  * You should then see a small _question mark_ (?) (_What does the selected code mean?_ ["**Explain this code**"]) to the left of the code, click it.
  * On the right hand side there will now be a small pop up to explain what your highlighted code does in natural language. Try highlighting other code sections as well.
  
  * At this point we should be fully aware of why and how our SQL injection vulnerability is occurring. Let's go ahead and make the fix. Use the **Edit** dropdown menu to **Open in Web IDE**

  * Replace the code on _line 111_ to this:

  ```plaintext
       cur.execute(query, (id,))
  ```
  Notice that the fix isn't exactly the same as the example fix.  We needed to change **user_id** to **id** since that is the argument being passed into the **select_note_by_id** function.

  * Now we can commit our change - **Note: use the commit dropdown menu to create a new branch to commit to**. Hit **Enter** to accept the default commit comment.
  * Use the button at the bottom of the ide window to **Create MR**.  You can keep all of the defaults on the _New Merge Request_ page and click **Create merge request**

    > [Docs for GitLab application security](https://docs.gitlab.com/ee/user/application_security/)

---

* [ ] **Step 4: Coding with [Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/)**
* ![code_suggestions_svg.svg](./code_suggestions_svg.svg)

  * Now lets actually use **Code Suggestions** to add a whole new class. We want to add a calculator class to this application so that we can enable calculations in the notes.
  * First, in the web IDE, right click the **notes** folder then click **new file**. Name this new file **_calc.py_**.
  
  * We then want to add the prompt below to let **Code Suggestions** know what we are trying to write:

  ```plaintext
   # define a calculator class that other functions can call
  ```
  * Press enter after the prompt and then wait for the suggestion to generate. As you are given suggestions, hit the TAB key to accept them. If it ever gets stuck try hitting the space bar or enter.
  * Code suggestions will write a very in depth calculator function and eventually will loop but feel free to stop it after 5 methods.
  * Code Suggestions doesn't just work for python files and supports multiple languages per project. Navigate into the **ai-sandbox/** folder for a list of currently up to date projects.
  * Choose one of the projects and test out code suggestions to write a hello world example or something more advanced.
  * Now we want to commit this code. Click **Source Control** Then use the commit dropdown menu to select **Commit to new branch**. Hit **Enter** to accept the default branch name and leave the commit comment empty.
 
  * On the bottom right corner, you will see a popup appear; click the **Create MR** button.  You can keep all of the defaults on the _New Merge Request_ page and click **Create merge request**

> [Docs for GitLab Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html)

  ---

* [ ] **Step 5: Automatically Generated Test Cases** 
* ![generate_tests.svg](./generate_tests.svg)
  * Now that we have added code, we also want to add some tests for it.
  * Starting in the merge request you created in Step 4, use the **Code** dropdown menu in the upper right-hand corner of the merge request and select **Open in Web IDE**  
  * In the **tests** folder, create a new file **test_calc.py**
  * Next, open the **notes/calc.py** file and highlight the class contents
  * Right click and use the **GitLab Duo Chat > Generate Tests** menu, at which point a pop up on the left hand side will appear to give you a number of suggestions of test cases you could add to your project for unit testing.
  * Copy the unit test cases code generated by GitLab Duo Chat and paste it into the **test_calc.py** file
  

  At the top of the file, paste in this code:
```
import os
import time
import notes
```

* Use the **Source Code** menu to commit your code to the same branch you used in Step 4.  
* Click the **Go to MR** button at the bottom of the page

> [Docs for GitLab Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html)


* [ ] **Step 6: AI in the Merge Request** 
* ![summarize_mr.svg](./summarize_mr.svg)
  * Now that we are back in our MR we should see that our code changes have kicked off another run of our pipeline. We have made a number of changes, so lets use the AI to generate a commit message.
  
  * After the pipeline has run and is _ready to merge_ (you should see a green checkmark followed by **Ready to merge!**; might require page refresh), select the **Edit commit message** checkbox.
  * You will see the **Create AI-generated commit message** button appear below so go ahead and click it. There may be a delay as you wait for the pipeline to complete; however, a quick write up of all the changes you made in the context of the merge request will be generated. This functionality also exists in the content of an issue. Click on **Insert** to apply the message.

  
  * This is the end of the hands on portion for this lab.

> Plan stage AI features typically have a start up time of 24 hours to train themselves, so if you want to see them in action make sure you start those tasks today

